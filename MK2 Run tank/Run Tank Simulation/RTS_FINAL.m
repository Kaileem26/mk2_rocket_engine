clear all;
clc;

L = readmatrix('L.txt');
V = readmatrix('V.txt');

% �(�) P(bar) r(kg/m^3) v(m^3/kg) u(kJ/kg) h(kJ/kg) s(kJ/kg/K) cv(kJ/kg/K)
% cp(kJ/kg/K) 
% Sound Spd.(m/s) 
% Joule-Thomson(K/bar) 
% Viscosity(uPa*s)
% Therm. Cond.(W/m*K) 
% Surf.Tension(N/m) (mono sto liquid)
% Phase

for i=8:10
    L(length(L),i) = L(length(L)-1,i);
    V(length(V),i) = V(length(V)-1,i);
end

L(length(L),:) = [];
V(length(V),:) = [];

T_ = L(:,1);
P_ = L(:,2).*1e5;
hl_ = L(:,6);
hv_ = V(:,6);
sl_ = L(:,7);
sv_ = V(:,7);
rl_ = L(:,3);
rv_ = V(:,3);
cv_ = V(:,8);
cp_ = V(:,9);
g_ = cp_./cv_;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

To = 273.15 + 25;
Avent = (2*1e-3)^2*pi/4;
Ainj = 14 * (2.5e-3)^2*pi/4;
dt = 0.01;
t = 0:dt:30;
tb = 3;

Vtank = 8.57e-3;
mo = 5.182;
Cdvent = 0.8;
Cdinj = 0.45;
Pout = 30e5;
Ppcs = 5e5;

To;
mo;
Po = interp1(T_,P_,To,'spline');
rlo = interp1(T_,rl_,To,'spline');
rvo = interp1(T_,rv_,To,'spline');
slo = interp1(T_,sl_,To,'spline');
svo = interp1(T_,sv_,To,'spline');
cvo = interp1(T_,cv_,To,'spline');
cpo = interp1(T_,cp_,To,'spline');
go = cpo/cvo;
xo = (rlo*rvo*Vtank - rvo*mo)/(mo*(rlo-rvo));
ro = rlo*rvo/( rlo*xo + rvo*(1-xo));
so = svo*xo + slo*(1-xo);
So = mo * so;
P = [];
x = [];
m = [];
g = [];
P_chocked = [];
P_chocked(1) = ( 2/ (go+1) )^ (go/(go-1)) * (Po*1e-5 -Ppcs*1e-5);
m(1) = mo;
P(1) = Po;
x(1) = xo;
g(1) = go;
 for i=1:length(t)
    if t(i) <= tb %auto to meros tou if einai mono gia vent
        mvent = Avent*Cdvent*sqrt( go * Po * rvo * (2/(go+1))^((go+1)/(go-1)));
        Sn = So - mvent*dt*svo;
        mn = mo - mvent*dt;
        sn = Sn/mn;
        rn = mn/Vtank;

        for j=1:length(sl_)
            if sn <= sl_(j)
                break
            end
            xi(j) = (sn-sl_(j))/(sv_(j)-sl_(j));
            ri(j) = rl_(j)*rv_(j)/( rl_(j)*xi(j) + rv_(j)*(1-xi(j))); 
            Ti(j) = T_(j);
            Pi(j) = P_(j);
        end

        Tn = interp1(ri,Ti,rn,'spline');
        Pn = interp1(ri,Pi,rn,'spline');
        xn = interp1(ri,xi,rn,'spline');
        rvn = interp1(T_,rv_,Tn,'spline');
        gn = interp1(T_,g_,Tn,'spline');
        svn = interp1(T_,sv_,Tn,'spline');

        Po = Pn;
        rvo = rvn;
        go = gn;
        svo = svn;
        So = Sn;
        mo = mn;

        P(i+1) = Pn;
        x(i+1) = xn;
        
    else %edw anoigei to valve gia ton injector
        mvent = Avent*Cdvent*sqrt( go * Po * rvo * (2/(go+1))^((go+1)/(go-1)));
        minj = Ainj*Cdinj * sqrt(2*rlo*(Po - Ppcs - Pout));
        Sn = So - mvent*dt*svo -minj*dt*slo;
        mn = mo - mvent*dt -minj*dt;
        sn = Sn/mn;
        rn = mn/Vtank;

        for j=1:length(sl_)
            if sn <= sl_(j)
                break
            end
            xi(j) = (sn-sl_(j))/(sv_(j)-sl_(j));
            ri(j) = rl_(j)*rv_(j)/( rl_(j)*xi(j) + rv_(j)*(1-xi(j))); 
            Ti(j) = T_(j);
            Pi(j) = P_(j);
        end
        
        Tn = interp1(ri,Ti,rn,'spline');
        Pn = interp1(ri,Pi,rn,'spline');
        xn = interp1(ri,xi,rn,'spline');
        rvn = interp1(T_,rv_,Tn,'spline');
        rln = interp1(T_,rl_,Tn,'spline');
        gn = interp1(T_,g_,Tn,'spline');
        svn = interp1(T_,sv_,Tn,'spline');
        sln = interp1(T_,sl_,Tn,'spline');

        Po = Pn;
        rvo = rvn;
        rlo = rln;
        go = gn;
        svo = svn;
        slo = sln;
        So = Sn;
        mo = mn;

        P(i+1) = Pn;
        x(i+1) = xn;
      end
    
    m(i+1) = mn;
    g(i+1) = gn;
    P_chocked(i+1) = ( 2/ (gn+1) )^ (gn/(gn-1)) * (Pn*1e-5-Ppcs*1e-5);
    if Tn <= 186
        break
    elseif Po -Ppcs - Pout <= 0 
        break
    elseif Pn <= 0
        break
%     elseif t(i)>= tb+3
%         break
    end
 end

 mdot = (m(2:end)-m(1:end-1))./dt;
for i=1:length(mdot)
    if mdot(i)>0
        mdot(i)=0;
    end
end

P(length(P))=[];
t(length(P)+1:length(t)) = [];
P = P.*1e-5;
figure
plot(t,P)
title('Pressure')

m(length(m))=[];
figure
plot(t,m)
title('mass')

figure
plot(t,mdot)
title('mass flow rate')

x(length(x))=[];
figure
plot(t,x)
title('quality')

% g(length(g))=[];
% figure
% plot(t,g)
% title('gamma')

% P_chocked(length(P_chocked))=[];
% figure
% plot(t,P_chocked)
% title('piesi meta ton injector gia na einai chocked')





