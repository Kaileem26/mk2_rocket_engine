# MK2 Engine   
This repository contains every file associated with the M-3000 MARK 2 engine.   

## Open Rocket   
In order to run the OR simulation, add the custom motors into the .ork file by opening it and then going into \edit\preferences\user defined thrust curves\ and add the two motors representing the actuall engine and the run tank's mass loss. Then restart Open Rocket.   

## License  
This project is lisenced under the [CERN OHLv1.2](https://gitlab.com/librespacefoundation/cronos-rocket/mk2-engine/-/blob/master/LICENSE)
